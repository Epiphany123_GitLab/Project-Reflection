import org.jetbrains.gradle.ext.Gradle

plugins {
    id 'java'
    id 'java-library'
    id 'maven-publish'
    id 'org.jetbrains.gradle.plugin.idea-ext' version '1.1.7'
    id 'eclipse'
    id 'com.gtnewhorizons.retrofuturagradle' version '1.3.35'
    id 'com.matthewprenger.cursegradle' version '1.4.0'
}

version = project.mod_version
group = project.maven_group
archivesBaseName = project.archives_base_name

// Set the toolchain version to decouple the Java we run Gradle with from the Java used to compile and run the mod
java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(8))
        // Azul covers the most platforms for Java 8 toolchains, crucially including MacOS arm64
        vendor.set(org.gradle.jvm.toolchain.JvmVendorSpec.AZUL)
    }
    // Generate sources and javadocs jars when building and publishing
    withSourcesJar()
    // withJavadocJar()
}

tasks.withType(JavaCompile).configureEach {
    options.encoding = 'UTF-8'
}

configurations {
    embed
    implementation.extendsFrom(embed)
}

minecraft {
    mcVersion = '1.12.2'

    // MCP Mappings
    mcpMappingChannel = 'stable'
    mcpMappingVersion = '39'
    
    // Set username here, the UUID will be looked up automatically
    username = 'Developer'
    
    // Add any additional tweaker classes here
    // extraTweakClasses.add('org.spongepowered.asm.launch.MixinTweaker')
    
    // Add various JVM arguments here for runtime
    def args = ["-ea:${project.group}"]
    if (project.use_coremod.toBoolean()) {
        args << '-Dfml.coreMods.load=' + coremod_plugin_class_name
    }
    if (project.use_mixins.toBoolean()) {
        args << '-Dmixin.hotSwap=true'
        args << '-Dmixin.checks.interfaces=true'
        args << '-Dmixin.debug.export=true'
    }
    extraRunJvmArguments.addAll(args)

    // Include and use dependencies' Access Transformer files
    useDependencyAccessTransformers = true
    
    // Add any properties you want to swap out for a dynamic value at build time here
    // Any properties here will be added to a class at build time, the name can be configured below
    // Example:
    // injectedTags.put('VERSION', project.version)
    // injectedTags.put('MOD_ID', project.archives_base_name)
}

// Generate a group.archives_base_name.Tags class
tasks.injectTags.configure {
    // Change Tags class' name here:
    outputClassName.set("${project.group}.${project.archives_base_name}.Tags")
}

repositories {
    maven {
        name 'CleanroomMC Maven'
        url 'https://maven.cleanroommc.com'
    }
    maven {
        name 'SpongePowered Maven'
        url 'https://repo.spongepowered.org/maven'
    }
    maven {
        name 'CurseMaven'
        url 'https://cursemaven.com'
        content {
            includeGroup 'curse.maven'
        }
    }
    maven{
        url "https://maven.covers1624.net"
    }
    maven{
        name 'thiakil'
        url 'https://maven.thiakil.com'
        content {
            includeGroup 'com.azanor.baubles'
        }
    }
    maven {
        url = 'https://maven.blamejared.com'
        name = 'BlameJared Maven'
    }
    mavenLocal() // Must be last for caching to work
}

dependencies {
    if (project.use_assetmover.toBoolean()) {
        implementation 'com.cleanroommc:assetmover:2.5'
    }
    /*
    if (project.use_mixins.toBoolean()) {
        implementation 'zone.rong:mixinbooter:8.8'
    }


     */
    compileOnly 'org.jetbrains:annotations:24.1.0'
    // Example of deobfuscating a dependency
    implementation "CraftTweaker2:CraftTweaker2-MC1120-Main:1.12-4.1.20.684"
    //implementation rfg.deobf(files('libs/vintagium-mc1.12.2-0.1.jar'))
    implementation rfg.deobf('curse.maven:codechickenlib-242818:2779848')
    implementation rfg.deobf("curse.maven:gregtech-ce-unofficial-557242:5121638")
    implementation rfg.deobf("curse.maven:gregtech-food-option-477021:5155451")
    implementation rfg.deobf("com.azanor.baubles:Baubles:1.12-1.5.2")
    implementation rfg.deobf("curse.maven:thaumcraft-223628:2629023")
    implementation rfg.deobf("curse.maven:thaumic-periphery-293505:2766866")
    implementation rfg.deobf("curse.maven:gcym-564858:4910674")
    implementation rfg.deobf("curse.maven:ceramics-250617:3158763")
    implementation rfg.deobf("curse.maven:groovyscript-687577:5120928")
    implementation rfg.deobf("curse.maven:abyssalcraft-53686:4856547")
    implementation rfg.deobf("curse.maven:acintegration-234851:3425537")
    implementation rfg.deobf("curse.maven:mysticalmechanics-300742:3006392")
    implementation rfg.deobf("curse.maven:had-enough-items-557549:4810661")
    implementation rfg.deobf("curse.maven:embers-extended-life-936489:5124633")
    implementation rfg.deobf("curse.maven:ae2-extended-life-570458:5147702")
    compileOnly rfg.deobf("curse.maven:ender-core-231868:4671384")
    implementation rfg.deobf("vazkii.botania:Botania:r1.10-364.5")
    implementation rfg.deobf("curse.maven:extrabotany-299086:3112313")
    implementation rfg.deobf("curse.maven:ae2-fluid-crafting-rework-623955:5310089")
    runtimeOnly(rfg.deobf("curse.maven:tickrate-changer-230233:2482684"))
    //compileOnly("curse.maven:gregtech-ce-unofficial-557242:5121638")
    implementation rfg.deobf(files("libs/Pollution-1.0.jar"))
    if (project.use_mixins.toBoolean()) {
        // Change your mixin refmap name here:
        String mixin = modUtils.enableMixins('zone.rong:mixinbooter:8.8', "mixins.${project.archives_base_name}.refmap.json")
        api (mixin) {
            transitive = false
        }
        annotationProcessor 'org.ow2.asm:asm-debug-all:5.2'
        annotationProcessor 'com.google.guava:guava:24.1.1-jre'
        annotationProcessor 'com.google.code.gson:gson:2.8.6'
        annotationProcessor (mixin) {
            transitive = false
        }
    }

}

// Adds Access Transformer files to tasks
if (project.use_access_transformer.toBoolean()) {
    for (File at : sourceSets.getByName("main").resources.files) {
        if (at.name.toLowerCase().endsWith("_at.cfg")) {
            tasks.deobfuscateMergedJarToSrg.accessTransformerFiles.from(at)
            tasks.srgifyBinpatchedJar.accessTransformerFiles.from(at)
        }
    }
}

processResources {
    // This will ensure that this task is redone when the versions change
    inputs.property 'version', project.version
    inputs.property 'mcversion', project.minecraft.version
    
    // Replace various properties in mcmod.info and pack.mcmeta if applicable
    filesMatching(['mcmod.info', 'pack.mcmeta']) { fcd ->
        // Replace version and mcversion
        fcd.expand (
                'version': project.version,
                'mcversion': project.minecraft.version
        )
    }
    
    if (project.use_access_transformer.toBoolean()) {
        rename '(.+_at.cfg)', 'META-INF/$1' // Make sure Access Transformer files are in META-INF folder
    }
}

jar {
    manifest {
        def attribute_map = [:]
        if (project.use_coremod.toBoolean()) {
            attribute_map['FMLCorePlugin'] = project.coremod_plugin_class_name
            if (project.include_mod.toBoolean()) {
                attribute_map['FMLCorePluginContainsFMLMod'] = true
                attribute_map['ForceLoadAsMod'] = project.gradle.startParameter.taskNames[0] == "build"
            }
        }
        if (project.use_access_transformer.toBoolean()) {
            attribute_map['FMLAT'] = project.archives_base_name + '_at.cfg'
        }
        attributes(attribute_map)
    }
    // Add all embedded dependencies into the jar
    from(provider{ configurations.embed.collect {it.isDirectory() ? it : zipTree(it)} })
}

idea {
    module {
        inheritOutputDirs = true
    }
    project {
        settings {
            runConfigurations {
                "1. Run Client"(Gradle) {
                    taskNames = ["runClient"]
                }
                "2. Run Server"(Gradle) {
                    taskNames = ["runServer"]
                }
                "3. Run Obfuscated Client"(Gradle) {
                    taskNames = ["runObfClient"]
                }
                "4. Run Obfuscated Server"(Gradle) {
                    taskNames = ["runObfServer"]
                }
            }
            compiler.javac {
                afterEvaluate {
                    javacAdditionalOptions = "-encoding utf8"
                    moduleJavacAdditionalOptions = [
                            (project.name + ".main"): tasks.compileJava.options.compilerArgs.collect { '"' + it + '"' }.join(' ')
                    ]
                }
            }
        }
    }
}

tasks.named("processIdeaSettings").configure {
    dependsOn("injectTags")
}
