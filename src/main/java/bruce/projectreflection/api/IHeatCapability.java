package bruce.projectreflection.api;

public interface IHeatCapability {
    int getTemperature();

    int getMaxTemperature();
}
