package bruce.projectreflection;

import gregtech.api.GTValues;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.launchwrapper.Launch;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

public class PRConstants {
    public static final String modid="projectreflection";
    public static final String[] V = Arrays.stream(GTValues.VN).map(String::toLowerCase).collect(Collectors.toList()).toArray(new String[0]);
    public static final boolean inDev = (Boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
    public static final CreativeTabs tab=new CreativeTabs("ProjectReflection") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(Blocks.COMMAND_BLOCK);
        }
    };
    public static final Random rand=new Random();
}
