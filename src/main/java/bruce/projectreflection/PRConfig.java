package bruce.projectreflection;

import net.minecraftforge.common.config.Config;

@Config(modid = PRConstants.modid)
public class PRConfig {
    public static double EU_TO_R = 4L;
    public static boolean disruptRecipes = false;
}